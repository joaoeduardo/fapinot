@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.groups') }}
@stop

@section('content')
<h1>{{ Lang::get('word.groups') }}</h1>
{{ HTML::linkAction('GroupController@create', Lang::get('word.create'), [], ['class' => 'btn btn-success']) }}
{{ Former::inline_open()->action(Request::path())->method('get')->addClass('pull-right') }}
{{ Former::text('search')->placeholder(Lang::get('word.search')) }}
{{ Former::close() }}
<table class="table">
	<thead>
		<tr>
			<th class="col-xs-4">{{ Lang::get('word.name') }}</th class="col-xs-">
			<th class="col-xs-2">{{ Lang::get('word.description') }}</th class="col-xs-">
			<th class="col-xs-2">{{ Lang::get('word.status') }}</th class="col-xs-">
			<th class="col-xs-4">{{ Lang::get('word.actions') }}</th class="col-xs-">
		</tr>
	</thead>
	<tbody>
		@foreach($groups as $group)
		<tr>
			<td>{{ $group->name }}</td>
			<td>{{ $group->description }}</td>
			<td>{{ $group->status ? Lang::get('word.active') : Lang::get('word.inactive') }}</td>
			<td>
				{{ Former::open()->controller('GroupController@destroy', [$group->id]) }}
				{{ HTML::linkAction('GroupController@edit', Lang::get('word.edit'), [$group->id], ['class' => 'btn btn-warning']) }}
				{{ Former::danger_submit('delete') }}
				{{ Former::close() }}
			</td>
		</tr>
		@endforeach
	</tbody>
	<tfoot>
		<th colspan="4" class="text-center">
			{{ Lang::get('word.pagination', ['from' => $groups->getFrom(), 'to' => $groups->getTo(), 'total' => $groups->getTotal()]) }}
		</th>
	</tfoot>
</table>
{{ $groups->links() }}
@stop