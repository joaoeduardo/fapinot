@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.dashboard') }}
@stop

@section('content')
<h1>{{ Lang::get('word.dashboard') }}</h1>
@stop