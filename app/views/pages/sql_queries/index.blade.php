@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.sqlqueries') }}
@stop

@section('content')
<h1>{{ Lang::get('word.sqlqueries') }}</h1>
{{ HTML::linkAction('SqlQueryController@create', Lang::get('word.create'), [$message->id], ['class' => 'btn btn-success']) }}
{{ Former::inline_open()->action(Request::path())->method('get')->addClass('pull-right') }}
{{ Former::text('search')->placeholder(Lang::get('word.search')) }}
{{ Former::close() }}
<table class="table">
	<thead>
		<tr>
			<th class="col-xs-4">{{ Lang::get('word.name') }}</th class="col-xs-">
			<th class="col-xs-4">{{ Lang::get('word.text') }}</th class="col-xs-">
			<th class="col-xs-4">{{ Lang::get('word.actions') }}</th class="col-xs-">
		</tr>
	</thead>
	<tbody>
		@foreach($sql_queries as $sql_query)
		<tr>
			<td>{{ $sql_query->name }}</td>
			<td>{{ $sql_query->text }}</td>
			<td>
				{{ Former::open()->controller('SqlQueryController@destroy', [$message->id, $sql_query->id]) }}
				{{ HTML::linkAction('SqlQueryController@edit', Lang::get('word.edit'), [$message->id, $sql_query->id], ['class' => 'btn btn-warning']) }}
				{{ Former::danger_submit('delete') }}
				{{ Former::close() }}
			</td>
		</tr>
		@endforeach
	</tbody>
	<tfoot>
		<th colspan="4" class="text-center">
			{{ Lang::get('word.pagination', ['from' => $sql_queries->getFrom(), 'to' => $sql_queries->getTo(), 'total' => $sql_queries->getTotal()]) }}
		</th>
	</tfoot>
</table>
{{ $sql_queries->links() }}
@stop