@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.login') }}
@stop

@section('content')
<h1>{{ Lang::get('word.login') }}</h1>
{{ Former::open()->controller('UserController@doLogin') }}
{{ Former::text('email') }}
{{ Former::password('password') }}
{{ Former::success_submit('login') }}
{{ Former::close() }}
@stop