<?php

class ImportController extends BaseController {

	public function showImport()
	{
		return View::make('pages.import.index');
	}

	public function doImport()
	{
		$rules = [
			'file' => [
				'required'
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator);
		} else {

			Excel::load(Input::file('file')->getRealPath(), function($reader) {

				$rows = $reader->all();

				foreach ($rows as $row) {

					$contact = new Contact();

					$contact->name      = $row['name'];
					$contact->email     = $row['email'] ? $row['email'] : '';
					$contact->cellphone = $row['cellphone'];
					$contact->status    = $row['status'] ? true : false;

					$contact->user()->associate(Auth::user());

					$contact->save();

					Eloquent::unguard();

					if($row['group']) {

						$group = Group::firstOrNew(['name' => $row['group']]);

						$group->status = isset($group->status) ? $group->status : true;

						$group->user()->associate(Auth::user());

						$group->save();

						$contact->groups()->sync([$group->id]);
					}
				}
			});

			Notification::success(Lang::get('word.success'));

			return Redirect::action('ImportController@showImport');
		}
	}
}