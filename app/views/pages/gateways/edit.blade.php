@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.edit') }} {{ Lang::get('word.gateway') }}
@stop

@section('content')
<h1>{{ Lang::get('word.edit') }} {{ Lang::get('word.gateway') }}</h1>
{{ Former::open()->controller('GatewayController@update', [$gateway->id]) }}
{{ Former::populate($gateway) }}
{{ Former::text('name') }}
{{ Former::text('url') }}
{{ Former::radios('status')->radios('inactive', 'active') }}
{{ Former::select('request_method_id')->label('request_method')->options($request_methods) }}
{{ Former::success_submit('save') }}
{{ Former::close() }}
@stop