<?php

class SqlQueryController extends BaseController {

	public function index($messageId)
	{
		$message = Auth::user()->messages()->find($messageId);

		if (Input::has('search')) {

			$search = Str::wrap(Input::get('search'));

			$sqlQueries = $message->sqlQueries()->where('name', 'like', $search)->paginate(5);
		} else {

			$sqlQueries = $message->sqlQueries()->paginate(5);
		}

		return View::make('pages.sql_queries.index')->with(['message' => $message, 'sql_queries' => $sqlQueries]);
	}

	public function create($messageId)
	{
		$message = Auth::user()->messages()->find($messageId);

		$sources = Auth::user()->sources()->whereStatus(true)->lists('name', 'id');

		return View::make('pages.sql_queries.create')->with(['message' => $message, 'sources' => $sources]);
	}

	public function store($messageId)
	{
		$rules = [
			'name' => [
				'required',
				'unique:sql_queries,name,NULL,id,deleted_at,NULL,user_id,' . Auth::user()->id
			],
			'text' => [
				'required'
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		} else {

			$message = Auth::user()->messages()->find($messageId);

			$source = Auth::user()->sources()->find(Input::get('source_id'));

			$sqlQuery = new SqlQuery();

			$sqlQuery->name = Input::get('name');
			$sqlQuery->text = Input::get('text');
			
			$sqlQuery->source()->associate($source);
			
			$sqlQuery->message()->associate($message);
			
			$sqlQuery->user()->associate(Auth::user());

			$sqlQuery->save();

			Notification::success(Lang::get('word.success'));

			return Redirect::action('SqlQueryController@index', $message->id);
		}
	}

	public function edit($messageId, $id)
	{
		$message = Auth::user()->messages()->find($messageId);

		$sources = Auth::user()->sources()->whereStatus(true)->lists('name', 'id');

		$sqlQuery = $message->sqlQueries()->with('source')->find($id);

		return View::make('pages.sql_queries.edit')->with(['message' => $message, 'sources' => $sources, 'sql_query' => $sqlQuery]);
	}

	public function update($messageId, $id)
	{
		$rules = [
			'name' => [
				'required',
				'unique:sql_queries,name,' . $id . ',id,deleted_at,NULL,user_id,' . Auth::user()->id
			],
			'text' => [
				'required'
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		} else {

			$message = Auth::user()->messages()->find($messageId);

			$source = Auth::user()->sources()->find(Input::get('source_id'));

			$sqlQuery = $message->sqlQueries()->find($id);

			$sqlQuery->name = Input::get('name');
			$sqlQuery->text = Input::get('text');
			
			$sqlQuery->source()->associate($source);
			
			$sqlQuery->message()->associate($message);
			
			$sqlQuery->user()->associate(Auth::user());

			$sqlQuery->save();

			Notification::success(Lang::get('word.success'));

			return Redirect::action('SqlQueryController@index', $message->id);
		}
	}

	public function destroy($messageId, $id)
	{

		$message = Auth::user()->messages()->find($messageId);

		$sqlQuery = $message->sqlQueries()->find($id);

		$sqlQuery->delete();

		Notification::success(Lang::get('word.success'));

		return Redirect::action('SqlQueryController@index', $message->id);
	}

}
