@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.arguments') }}
@stop

@section('content')
<h1>{{ Lang::get('word.arguments') }}</h1>
{{ HTML::linkAction('ArgumentController@create', Lang::get('word.create'), [$gateway->id], ['class' => 'btn btn-success']) }}
{{ Former::inline_open()->action(Request::path())->method('get')->addClass('pull-right') }}
{{ Former::text('search')->placeholder(Lang::get('word.search')) }}
{{ Former::close() }}
<table class="table">
	<thead>
		<tr>
			<th class="col-xs-4">{{ Lang::get('word.name') }}</th class="col-xs-">
			<th class="col-xs-4">{{ Lang::get('word.value') }}</th class="col-xs-">
			<th class="col-xs-4">{{ Lang::get('word.actions') }}</th class="col-xs-">
		</tr>
	</thead>
	<tbody>
		@foreach($arguments as $argument)
		<tr>
			<td>{{ $argument->name }}</td>
			<td>{{ $argument->value }}</td>
			<td>
				{{ Former::open()->controller('ArgumentController@destroy', [$gateway->id, $argument->id]) }}
				{{ HTML::linkAction('ArgumentController@edit', Lang::get('word.edit'), [$gateway->id, $argument->id], ['class' => 'btn btn-warning']) }}
				{{ Former::danger_submit('delete') }}
				{{ Former::close() }}
			</td>
		</tr>
		@endforeach
	</tbody>
	<tfoot>
		<th colspan="4" class="text-center">
			{{ Lang::get('word.pagination', ['from' => $arguments->getFrom(), 'to' => $arguments->getTo(), 'total' => $arguments->getTotal()]) }}
		</th>
	</tfoot>
</table>
{{ $arguments->links() }}
@stop