@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.edit') }} {{ Lang::get('word.group') }}
@stop

@section('content')
<h1>{{ Lang::get('word.edit') }} {{ Lang::get('word.group') }}</h1>
{{ Former::open()->controller('GroupController@update', [$group->id]) }}
{{ Former::populate($group) }}
{{ Former::text('name') }}
{{ Former::textarea('description') }}
{{ Former::radios('status')->radios('inactive', 'active') }}
{{ Former::multiselect('contacts')->options($contacts) }}
{{ Former::success_submit('save') }}
{{ Former::close() }}
@stop