@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.create') }} {{ Lang::get('word.cronjob') }}
@stop

@section('content')
<h1>{{ Lang::get('word.create') }} {{ Lang::get('word.cronjob') }}</h1>
{{ Former::open()->controller('CronjobController@store', [$message->id]) }}
{{ Former::text('name') }}
{{ Former::text('text') }}
{{ Former::success_submit('save') }}
{{ Former::close() }}
@stop