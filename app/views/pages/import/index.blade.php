@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.import') }}
@stop

@section('content')
<h1>{{ Lang::get('word.import') }}</h1>
{{ Former::open_for_files()->controller('ImportController@doImport') }}
{{ Former::file('file') }}
{{ Former::success_submit('save') }}
{{ Former::close() }}
@stop