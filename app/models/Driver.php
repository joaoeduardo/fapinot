<?php

class Driver extends Eloquent {

	public $timestamps = false;

	public function sources()
	{
		return $this->hasMany('Source');
	}

}
