<?php

class Contact extends Eloquent {

	use SoftDeletingTrait;

	public function externalContact()
	{
		return $this->hasOne('ExternalContact');
	}

	public function groups()
	{
		return $this->belongsToMany('Group');
	}

	public function messages()
	{
		return $this->belongsToMany('Message');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

}
