<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArgumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(
			'arguments',
			function(Blueprint $table) {

				$table->bigIncrements('id');
				$table->string('name');
				$table->string('value');
				$table->bigInteger('gateway_id')->unsigned();
				$table->foreign('gateway_id')->references('id')->on('gateways');
				$table->bigInteger('user_id')->unsigned();
				$table->foreign('user_id')->references('id')->on('users');
				$table->timestamps();
				$table->softDeletes();
			}
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('arguments');
	}

}
