<?php

Event::listen('cron.collectJobs', function() {

	$cronjobs = Cronjob::all();

	foreach ($cronjobs as $cronjob) {

		Cron::add((string) $cronjob->id, $cronjob->text, function() use ($cronjob) {

			$cronjob->message->send();

			return null;
		});
	}
});
