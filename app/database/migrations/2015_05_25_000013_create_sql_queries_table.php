<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSqlQueriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(
			'sql_queries',
			function(Blueprint $table) {
				
				$table->bigIncrements('id');
				$table->string('name');
				$table->string('text');
				$table->bigInteger('source_id')->unsigned();
				$table->foreign('source_id')->references('id')->on('sources');
				$table->bigInteger('message_id')->unsigned();
				$table->foreign('message_id')->references('id')->on('messages');
				$table->bigInteger('user_id')->unsigned();
				$table->foreign('user_id')->references('id')->on('users');
				$table->timestamps();
				$table->softDeletes();
			}
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('sql_queries');
	}

}
