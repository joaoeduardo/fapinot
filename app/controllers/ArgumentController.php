<?php

class ArgumentController extends BaseController {

	public function index($gatewayId)
	{
		$gateway = Auth::user()->gateways()->find($gatewayId);

		if (Input::has('search')) {

			$search = Str::wrap(Input::get('search'));

			$arguments = $gateway->arguments()->where('name', 'like', $search)->paginate(5);
		} else {

			$arguments = $gateway->arguments()->paginate(5);
		}

		return View::make('pages.arguments.index')->with(['gateway' => $gateway, 'arguments' => $arguments]);
	}

	public function create($gatewayId)
	{
		$gateway = Auth::user()->gateways()->find($gatewayId);

		return View::make('pages.arguments.create')->with(['gateway' => $gateway]);
	}

	public function store($gatewayId)
	{
		$rules = [
			'name' => [
				'required',
				'unique:arguments,name,NULL,id,deleted_at,NULL,user_id,' . Auth::user()->id . ',gateway_id,' . $gatewayId
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		} else {

			$gateway = Auth::user()->gateways()->find($gatewayId);

			$argument = new Argument();

			$argument->name  = Input::get('name');
			$argument->value = Input::get('value');

			$argument->gateway()->associate($gateway);

			$argument->user()->associate(Auth::user());

			$argument->save();

			Notification::success(Lang::get('word.success'));

			return Redirect::action('ArgumentController@index', $gateway->id);
		}
	}

	public function edit($gatewayId, $id)
	{
		$gateway = Auth::user()->gateways()->find($gatewayId);

		$argument = $gateway->arguments()->find($id);

		return View::make('pages.arguments.edit')->with(['gateway' => $gateway, 'argument' => $argument]);
	}

	public function update($gatewayId, $id)
	{
		$rules = [
			'name' => [
				'required',
				'unique:arguments,name,' . $id . ',id,deleted_at,NULL,user_id,' . Auth::user()->id . ',gateway_id,' . $gatewayId
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		} else {

			$gateway = Auth::user()->gateways()->find($gatewayId);

			$argument = $gateway->arguments()->find($id);

			$argument->name  = Input::get('name');
			$argument->value = Input::get('value');

			$argument->gateway()->associate($gateway);

			$argument->user()->associate(Auth::user());

			$argument->save();

			Notification::success(Lang::get('word.success'));

			return Redirect::action('ArgumentController@index', $gateway->id);
		}
	}

	public function destroy($gatewayId, $id)
	{
		$gateway = Auth::user()->gateways()->find($gatewayId);

		$argument = $gateway->arguments()->find($id);

		$argument->delete();

		Notification::success(Lang::get('word.success'));

		return Redirect::action('ArgumentController@index', $gateway->id);
	}

}
