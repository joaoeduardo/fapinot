<?php

Route::get('/', 'HomeController@index');

Route::group(['prefix' => 'user'], function() {
	Route::get('login', 'UserController@showLogin');
	Route::post('login', 'UserController@doLogin');
	Route::get('logout', 'UserController@doLogout');
	Route::get('signup', 'UserController@showSignup');
	Route::post('signup', 'UserController@doSignup');
});

Route::group(['prefix' => 'dashboard', 'before' => 'auth'], function() {
	Route::get('/', 'DashboardController@index');
	Route::resource('contact', 'ContactController');
	Route::resource('group', 'GroupController');
	Route::resource('gateway', 'GatewayController');
	Route::resource('gateway.argument', 'ArgumentController');
	Route::resource('source', 'SourceController');
	Route::resource('message', 'MessageController');
	Route::resource('message.cron', 'CronjobController');
	Route::resource('message.query', 'SqlQueryController');
	Route::get('message/{id}/send', 'MessageController@send');
	Route::get('import', 'ImportController@showImport');
	Route::post('import', 'ImportController@doImport');
});