@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.home') }}
@stop

@section('content')
<h1>{{ Lang::get('word.home') }}</h1>
@stop