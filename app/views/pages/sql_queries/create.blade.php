@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.create') }} {{ Lang::get('word.sqlquery') }}
@stop

@section('content')
<h1>{{ Lang::get('word.create') }} {{ Lang::get('word.sqlquery') }}</h1>
{{ Former::open()->controller('SqlQueryController@store', [$message->id]) }}
{{ Former::text('name') }}
{{ Former::textarea('text') }}
{{ Former::select('source_id')->label('source')->options($sources) }}
{{ Former::success_submit('save') }}
{{ Former::close() }}
@stop