<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGatewaysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(
			'gateways',
			function(Blueprint $table) {

				$table->bigIncrements('id');
				$table->string('name');
				$table->string('url');
				$table->boolean('status');
				$table->bigInteger('request_method_id')->unsigned();
				$table->foreign('request_method_id')->references('id')->on('request_methods');
				$table->bigInteger('user_id')->unsigned();
				$table->foreign('user_id')->references('id')->on('users');
				$table->timestamps();
				$table->softDeletes();
			}
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('gateways');
	}

}
