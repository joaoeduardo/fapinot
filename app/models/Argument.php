<?php

class Argument extends Eloquent {

	use SoftDeletingTrait;

	public function gateway()
	{
		return $this->belongsTo('Gateway');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

}
