@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.create') }} {{ Lang::get('word.argument') }}
@stop

@section('content')
<h1>{{ Lang::get('word.create') }} {{ Lang::get('word.argument') }}</h1>
{{ Former::open()->controller('ArgumentController@store', [$gateway->id]) }}
{{ Former::text('name') }}
{{ Former::text('value') }}
{{ Former::success_submit('save') }}
{{ Former::close() }}
@stop