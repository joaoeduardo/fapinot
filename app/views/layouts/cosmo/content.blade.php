<div class="container">
	{{ Notification::showAll() }}
	<div class="row">
		<div class="col-xs-12">
			@yield('content')
		</div>
	</div>
</div>