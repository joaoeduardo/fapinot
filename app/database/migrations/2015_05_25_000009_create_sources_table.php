<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourcesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(
			'sources',
			function(Blueprint $table) {

				$table->bigIncrements('id');
				$table->string('name');
				$table->string('host');
				$table->string('username');
				$table->string('password');
				$table->string('db');
				$table->boolean('status');
				$table->bigInteger('driver_id')->unsigned();
				$table->foreign('driver_id')->references('id')->on('drivers');
				$table->bigInteger('user_id')->unsigned();
				$table->foreign('user_id')->references('id')->on('users');
				$table->timestamps();
				$table->softDeletes();
			}
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('sources');
	}

}
