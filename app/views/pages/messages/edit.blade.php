@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.edit') }} {{ Lang::get('word.message') }}
@stop

@section('content')
<h1>{{ Lang::get('word.edit') }} {{ Lang::get('word.message') }}</h1>
{{ Former::open()->controller('MessageController@update', [$message->id]) }}
{{ Former::populate($message) }}
{{ Former::text('name') }}
{{ Former::textarea('content') }}
{{ Former::select('gateway_id')->label('gateway')->options($gateways) }}
{{ Former::multiselect('contacts')->options($contacts) }}
{{ Former::success_submit('save') }}
{{ Former::close() }}
@stop