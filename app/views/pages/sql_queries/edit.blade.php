@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.edit') }} {{ Lang::get('word.sqlquery') }}
@stop

@section('content')
<h1>{{ Lang::get('word.edit') }} {{ Lang::get('word.sqlquery') }}</h1>
{{ Former::open()->controller('SqlQueryController@update', [$message->id, $sql_query->id]) }}
{{ Former::populate($sql_query) }}
{{ Former::text('name') }}
{{ Former::textarea('text') }}
{{ Former::select('source_id')->label('source')->options($sources) }}
{{ Former::success_submit('save') }}
{{ Former::close() }}
@stop