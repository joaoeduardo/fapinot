@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.contacts') }}
@stop

@section('content')
<h1>{{ Lang::get('word.contacts') }}</h1>
{{ HTML::linkAction('ContactController@create', Lang::get('word.create'), [], ['class' => 'btn btn-success']) }}
{{ Former::inline_open()->action(Request::path())->method('get')->addClass('pull-right') }}
{{ Former::text('search')->placeholder(Lang::get('word.search')) }}
{{ Former::close() }}
<table class="table">
	<thead>
		<tr>
			<th class="col-xs-4">{{ Lang::get('word.name') }}</th class="col-xs-">
			<th class="col-xs-2">{{ Lang::get('word.groups') }}</th class="col-xs-">
			<th class="col-xs-2">{{ Lang::get('word.status') }}</th class="col-xs-">
			<th class="col-xs-4">{{ Lang::get('word.actions') }}</th class="col-xs-">
		</tr>
	</thead>
	<tbody>
		@foreach($contacts as $contact)
		<tr>
			<td>{{ $contact->name }}</td>
			<td>{{ str_limit($contact->groups->implode('name', ', '), 25, '...') }}</td>
			<td>{{ $contact->status ? Lang::get('word.active') : Lang::get('word.inactive') }}</td>
			<td>
				{{ Former::open()->controller('ContactController@destroy', [$contact->id]) }}
				{{ HTML::linkAction('ContactController@edit', Lang::get('word.edit'), [$contact->id], ['class' => 'btn btn-warning']) }}
				{{ Former::danger_submit('delete') }}
				{{ Former::close() }}
			</td>
		</tr>
		@endforeach
	</tbody>
	<tfoot>
		<th colspan="4" class="text-center">
			{{ Lang::get('word.pagination', ['from' => $contacts->getFrom(), 'to' => $contacts->getTo(), 'total' => $contacts->getTotal()]) }}
		</th>
	</tfoot>
</table>
{{ $contacts->links() }}
@stop