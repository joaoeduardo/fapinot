@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.messages') }}
@stop

@section('content')
<h1>{{ Lang::get('word.messages') }}</h1>
{{ HTML::linkAction('MessageController@create', Lang::get('word.create'), [], ['class' => 'btn btn-success']) }}
{{ Former::inline_open()->action(Request::path())->method('get')->addClass('pull-right') }}
{{ Former::text('search')->placeholder(Lang::get('word.search')) }}
{{ Former::close() }}
<table class="table">
	<thead>
		<tr>
			<th class="col-xs-7">{{ Lang::get('word.name') }}</th class="col-xs-">
			<th class="col-xs-5">{{ Lang::get('word.actions') }}</th class="col-xs-">
		</tr>
	</thead>
	<tbody>
		@foreach($messages as $message)
		<tr>
			<td>{{ $message->name }}</td>
			<td>
				{{ Former::open()->controller('MessageController@destroy', [$message->id]) }}
				{{ HTML::linkAction('CronjobController@index', Lang::get('word.cronjobs'), [$message->id], ['class' => 'btn btn-info']) }}
				{{ HTML::linkAction('SqlQueryController@index', Lang::get('word.sqlqueries'), [$message->id], ['class' => 'btn btn-info']) }}
				{{ HTML::linkAction('MessageController@send', Lang::get('word.send'), [$message->id], ['class' => 'btn btn-info']) }}
				{{ HTML::linkAction('MessageController@edit', Lang::get('word.edit'), [$message->id], ['class' => 'btn btn-warning']) }}
				{{ Former::danger_submit('delete') }}
				{{ Former::close() }}
			</td>
		</tr>
		@endforeach
	</tbody>
	<tfoot>
		<th colspan="4" class="text-center">
			{{ Lang::get('word.pagination', ['from' => $messages->getFrom(), 'to' => $messages->getTo(), 'total' => $messages->getTotal()]) }}
		</th>
	</tfoot>
</table>
{{ $messages->links() }}
@stop