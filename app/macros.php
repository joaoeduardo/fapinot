<?php

Str::macro('wrap', function($content, $wrapper = '%') {

	return $wrapper . $content . $wrapper;
});
