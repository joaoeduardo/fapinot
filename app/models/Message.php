<?php

class Message extends Eloquent {

	use SoftDeletingTrait;

	public function contacts()
	{
		return $this->belongsToMany('Contact');
	}

	public function cronjobs()
	{
		return $this->hasMany('Cronjob');
	}

	public function gateway()
	{
		return $this->belongsTo('Gateway');
	}

	public function sqlQueries()
	{
		return $this->hasMany('SqlQuery');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function send()
	{
		if ($this->sqlQueries->count()) {

			foreach ($this->sqlQueries as $sqlQuery) {
				
				$results = $sqlQuery->execute();

				foreach ($results as $result) {

					Eloquent::unguard();

					$externalContact = ExternalContact::firstOrNew([
						'source_id' => $sqlQuery->source->id,
						'remote_id' => $result['id']
					]);

					$this->contacts->add($externalContact->import($result));

					$externalContact->save();
				}
			}
		}

		foreach ($this->contacts as $contact) {

			$this->gateway->request($contact->cellphone, $this->content);
		}
	}

}
