<?php

class UserController extends BaseController {

	public function showLogin()
	{
		return View::make('pages.user.login');
	}

	public function doLogin()
	{
		$data = [
			'email'      => Input::get('email'),
			'password'   => Input::get('password'),
			'deleted_at' => NULL
		];

		$rules = [
			'email' => [
				'required',
				'email'
			],
			'password' => [
				'required'
			]
		];

		$validator = Validator::make($data, $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::except('password'));
		} else {

			if (Auth::attempt($data)) {

				Notification::success(Lang::get('word.login_success'));

				return Redirect::intended(URL::action('DashboardController@index'));
			} else {

				Notification::error(Lang::get('word.login_error'));

				return Redirect::back();
			}
		}
	}

	public function doLogout()
	{
		Auth::logout();

		Notification::success(Lang::get('word.logout_success'));
		
		return Redirect::action('HomeController@index');
	}

	public function showSignup()
	{
		return View::make('pages.user.signup');
	}

	public function doSignup()
	{
		$rules = [
			'firstname' => [
				'required'
			],
			'lastname' => [
				'required'
			],
			'nickname' => [
				'required'
			],
			'email' => [
				'required',
				'email',
				'unique:users,email,NULL,id,deleted_at,NULL'
			],
			'password' => [
				'required'
			],
			'repeat-password' => [
				'required',
				'same:password'
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::except('password'));
		} else {

			$user = new User();

			$user->firstname = Input::get('firstname');
			$user->lastname  = Input::get('lastname');
			$user->nickname  = Input::get('nickname');
			$user->email     = Input::get('email');
			$user->password  = Hash::make(Input::get('password'));

			$user->save();

			Notification::success(Lang::get('word.signup_success'));

			return Redirect::action('HomeController@index');
		}
	}

}
