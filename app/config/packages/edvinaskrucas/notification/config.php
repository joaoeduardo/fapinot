<?php

return array(
	'default_format' => array(
		'default' => '<div class="row"><div class="col-xs-12"><div class="alert alert-:type">:message</div></div></div>',
	),
	'default_formats' => array(
		'default' => array(
			'error' => '<div class="row"><div class="col-xs-12"><div class="alert alert-danger">:message</div></div></div>',
		),
	),
);
