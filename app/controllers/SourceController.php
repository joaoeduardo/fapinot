<?php

class SourceController extends BaseController {

	public function index()
	{
		if (Input::has('search')) {

			$search = Str::wrap(Input::get('search'));

			$sources = Auth::user()->sources()->where('name', 'like', $search)->paginate(5);
		} else {

			$sources = Auth::user()->sources()->paginate(5);
		}

		return View::make('pages.sources.index')->with(['sources' => $sources]);
	}

	public function create()
	{
		$drivers = Driver::lists('name', 'id');

		return View::make('pages.sources.create')->with(['drivers' => $drivers]);
	}

	public function store()
	{
		$rules = [
			'name' => [
				'required',
				'unique:sources,name,NULL,id,deleted_at,NULL,user_id,' . Auth::user()->id
			],
			'host' => [
				'required'
			],
			'username' => [
				'required'
			],
			'db' => [
				'required'
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		} else {

			$driver = Driver::find(Input::get('driver_id'));

			$source = new Source();

			$source->name     = Input::get('name');
			$source->host     = Input::get('host');
			$source->username = Input::get('username');
			$source->password = Input::get('password');
			$source->db       = Input::get('db');
			$source->status   = Input::get('status');

			$source->driver()->associate($driver);

			$source->user()->associate(Auth::user());

			$source->save();

			Notification::success(Lang::get('word.success'));

			return Redirect::action('SourceController@index');
		}
	}

	public function edit($id)
	{
		$source = Auth::user()->sources()->with('driver')->find($id);

		$drivers = Driver::lists('name', 'id');

		return View::make('pages.sources.edit')->with(['source' => $source, 'drivers' => $drivers]);
	}

	public function update($id)
	{
		$rules = [
			'name' => [
				'required',
				'unique:sources,name,' . $id . ',id,deleted_at,NULL,user_id,' . Auth::user()->id
			],
			'host' => [
				'required'
			],
			'username' => [
				'required'
			],
			'db' => [
				'required'
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		} else {

			$driver = Driver::find(Input::get('driver_id'));

			$source = Auth::user()->sources()->find($id);

			$source->name     = Input::get('name');
			$source->host     = Input::get('host');
			$source->username = Input::get('username');
			$source->password = Input::get('password');
			$source->db       = Input::get('db');
			$source->status   = Input::get('status');
			
			$source->driver()->associate($driver);
			
			$source->user()->associate(Auth::user());

			$source->save();

			Notification::success(Lang::get('word.success'));

			return Redirect::action('SourceController@index');
		}
	}

	public function destroy($id)
	{
		$source = Auth::user()->sources()->find($id);

		$source->sqlQueries()->delete();

		$source->delete();

		Notification::success(Lang::get('word.success'));

		return Redirect::action('SourceController@index');
	}

}
