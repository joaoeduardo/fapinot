<?php

class ExternalContact extends Eloquent {

	public $timestamps = false;

	public function contact()
	{
		return $this->belongsTo('Contact');
	}

	public function source()
	{
		return $this->belongsTo('Source');
	}

	public function import($remote)
	{
		if ($this->exists) {

			$contact = $this->contact;
		} else {

			$contact = new Contact();
		}

		$contact->name      = $remote['name'];
		$contact->cellphone = $remote['cellphone'];
		$contact->email     = $remote['email'];
		$contact->status    = true;

		$contact->user()->associate(Auth::user());

		$contact->save();

		$this->contact()->associate($contact);

		return $this->contact;
	}

}
