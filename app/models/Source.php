<?php

class Source extends Eloquent {

	use SoftDeletingTrait;

	public function driver()
	{
		return $this->belongsTo('Driver');
	}

	public function externalContacts()
	{
		return $this->hasMany('ExternalContact');
	}

	public function sqlQueries()
	{
		return $this->hasMany('SqlQuery');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function connect()
	{
		$capsule = new Illuminate\Database\Capsule\Manager();

		$capsule->addConnection([
			'driver'    => $this->driver->prefix,
			'host'      => $this->host,
			'database'  => $this->db,
			'username'  => $this->username,
			'password'  => $this->password,
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		]);

		return $capsule->getConnection();
	}

}
