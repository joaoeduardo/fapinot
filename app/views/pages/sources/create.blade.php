@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.create') }} {{ Lang::get('word.source') }}
@stop

@section('content')
<h1>{{ Lang::get('word.create') }} {{ Lang::get('word.source') }}</h1>
{{ Former::open()->controller('SourceController@store') }}
{{ Former::text('name') }}
{{ Former::text('host') }}
{{ Former::text('username') }}
{{ Former::password('password') }}
{{ Former::text('db') }}
{{ Former::radios('status')->radios('inactive', 'active')->check(1) }}
{{ Former::select('driver_id')->label('driver')->options($drivers) }}
{{ Former::success_submit('save') }}
{{ Former::close() }}
@stop