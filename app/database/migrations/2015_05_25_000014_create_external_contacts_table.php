<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExternalContactsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(
			'external_contacts',
			function(Blueprint $table) {

				$table->bigIncrements('id');
				$table->bigInteger('remote_id')->unsigned();
				$table->bigInteger('contact_id')->unsigned();
				$table->foreign('contact_id')->references('id')->on('contacts');
				$table->bigInteger('source_id')->unsigned();
				$table->foreign('source_id')->references('id')->on('sources');
			}
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('external_contacts');
	}

}
