<?php

class RequestMethod extends Eloquent {

	public $timestamps = false;

	public function gateways()
	{
		return $this->hasMany('Gateway');
	}

}
