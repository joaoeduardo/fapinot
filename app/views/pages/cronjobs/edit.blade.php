@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.edit') }} {{ Lang::get('word.cronjob') }}
@stop

@section('content')
<h1>{{ Lang::get('word.edit') }} {{ Lang::get('word.cronjob') }}</h1>
{{ Former::open()->controller('CronjobController@update', [$message->id, $cronjob->id]) }}
{{ Former::populate($cronjob) }}
{{ Former::text('name') }}
{{ Former::text('text') }}
{{ Former::success_submit('save') }}
{{ Former::close() }}
@stop