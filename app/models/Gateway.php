<?php

class Gateway extends Eloquent {

	use SoftDeletingTrait;

	public function arguments()
	{
		return $this->hasMany('Argument');
	}

	public function messages()
	{
		return $this->hasMany('Message');
	}

	public function requestMethod()
	{
		return $this->belongsTo('RequestMethod');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function request($cellphone, $message)
	{
		$search = [
			'{cellphone}',
			'{message}'
		];

		$replace = [
			$cellphone,
			$message
		];

		$query = [];

		foreach ($this->arguments as $argument) {
			
			$query[$argument->name] = str_replace($search, $replace, $argument->value);
		}

		$httpMethod = strtolower($this->requestMethod->name);

		$curl = new Alexsoft\Curl($this->url);

		$curl->addData($query);

		$response = $curl->$httpMethod();
	}

}
