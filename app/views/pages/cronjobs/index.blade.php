@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.cronjobs') }}
@stop

@section('content')
<h1>{{ Lang::get('word.cronjobs') }}</h1>
{{ HTML::linkAction('CronjobController@create', Lang::get('word.create'), [$message->id], ['class' => 'btn btn-success']) }}
{{ Former::inline_open()->action(Request::path())->method('get')->addClass('pull-right') }}
{{ Former::text('search')->placeholder(Lang::get('word.search')) }}
{{ Former::close() }}
<table class="table">
	<thead>
		<tr>
			<th class="col-xs-4">{{ Lang::get('word.name') }}</th class="col-xs-">
			<th class="col-xs-4">{{ Lang::get('word.text') }}</th class="col-xs-">
			<th class="col-xs-4">{{ Lang::get('word.actions') }}</th class="col-xs-">
		</tr>
	</thead>
	<tbody>
		@foreach($cronjobs as $cronjob)
		<tr>
			<td>{{ $cronjob->name }}</td>
			<td>{{ $cronjob->text }}</td>
			<td>
				{{ Former::open()->controller('CronjobController@destroy', [$message->id, $cronjob->id]) }}
				{{ HTML::linkAction('CronjobController@edit', Lang::get('word.edit'), [$message->id, $cronjob->id], ['class' => 'btn btn-warning']) }}
				{{ Former::danger_submit('delete') }}
				{{ Former::close() }}
			</td>
		</tr>
		@endforeach
	</tbody>
	<tfoot>
		<th colspan="4" class="text-center">
			{{ Lang::get('word.pagination', ['from' => $cronjobs->getFrom(), 'to' => $cronjobs->getTo(), 'total' => $cronjobs->getTotal()]) }}
		</th>
	</tfoot>
</table>
{{ $cronjobs->links() }}
@stop