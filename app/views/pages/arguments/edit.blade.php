@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.edit') }} {{ Lang::get('word.argument') }}
@stop

@section('content')
<h1>{{ Lang::get('word.edit') }} {{ Lang::get('word.argument') }}</h1>
{{ Former::open()->controller('ArgumentController@update', [$gateway->id, $argument->id]) }}
{{ Former::populate($argument) }}
{{ Former::text('name') }}
{{ Former::text('value') }}
{{ Former::success_submit('save') }}
{{ Former::close() }}
@stop