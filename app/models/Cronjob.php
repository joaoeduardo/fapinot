<?php

class Cronjob extends Eloquent {

	use SoftDeletingTrait;

	public function message()
	{
		return $this->belongsTo('Message');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

}
