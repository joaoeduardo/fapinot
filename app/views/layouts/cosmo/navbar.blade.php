<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
				<span class="sr-only">{{ Lang::get('word.toggle') }}</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			{{ HTML::linkAction('HomeController@index', 'Fapinot', [], ['class' => 'navbar-brand']) }}
		</div>
		<div class="collapse navbar-collapse" id="navbar-collapse">
			@if (Auth::check())
				<ul class="nav navbar-nav">
					<li>{{ HTML::linkAction('DashboardController@index', Lang::get('word.dashboard')) }}</li>
					<li>{{ HTML::linkAction('ContactController@index', Lang::get('word.contacts')) }}</li>
					<li>{{ HTML::linkAction('GroupController@index', Lang::get('word.groups')) }}</li>
					<li>{{ HTML::linkAction('SourceController@index', Lang::get('word.sources')) }}</li>
					<li>{{ HTML::linkAction('GatewayController@index', Lang::get('word.gateways')) }}</li>
					<li>{{ HTML::linkAction('MessageController@index', Lang::get('word.messages')) }}</li>
					<li>{{ HTML::linkAction('ImportController@showImport', Lang::get('word.imports')) }}</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>{{ HTML::linkAction('UserController@doLogout', Lang::get('word.logout')) }}</li>
				</ul>
			@else
				<ul class="nav navbar-nav navbar-right">
					<li>{{ HTML::linkAction('UserController@showSignup', Lang::get('word.signup')) }}</li>
					<li>{{ HTML::linkAction('UserController@showLogin', Lang::get('word.login')) }}</li>
				</ul>
			@endif
		</div>
	</div>
</nav>