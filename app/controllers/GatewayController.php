<?php

class GatewayController extends BaseController {

	public function index()
	{
		if (Input::has('search')) {

			$search = Str::wrap(Input::get('search'));

			$gateways = Auth::user()->gateways()->where('name', 'like', $search)->paginate(5);
		} else {

			$gateways = Auth::user()->gateways()->paginate(5);
		}

		return View::make('pages.gateways.index')->with(['gateways' => $gateways]);
	}

	public function create()
	{
		$requestMethods = RequestMethod::lists('name', 'id');

		return View::make('pages.gateways.create')->with(['request_methods' => $requestMethods]);
	}

	public function store()
	{
		$rules = [
			'name' => [
				'required',
				'unique:gateways,name,NULL,id,deleted_at,NULL,user_id,' . Auth::user()->id
			],
			'url' => [
				'required'
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		} else {

			$requestMethod = RequestMethod::find(Input::get('request_method_id'));

			$gateway = new Gateway();

			$gateway->name   = Input::get('name');
			$gateway->url    = Input::get('url');
			$gateway->status = Input::get('status');

			$gateway->requestMethod()->associate($requestMethod);

			$gateway->user()->associate(Auth::user());

			$gateway->save();

			Notification::success(Lang::get('word.success'));

			return Redirect::action('GatewayController@index');
		}
	}

	public function edit($id)
	{
		$gateway = Auth::user()->gateways()->with('requestMethod')->find($id);

		$requestMethods = RequestMethod::lists('name', 'id');

		return View::make('pages.gateways.edit')->with(['gateway' => $gateway, 'request_methods' => $requestMethods]);
	}

	public function update($id)
	{
		$rules = [
			'name' => [
				'required',
				'unique:gateways,name,' . $id . ',id,deleted_at,NULL,user_id,' . Auth::user()->id
			],
			'url' => [
				'required'
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		} else {

			$requestMethod = RequestMethod::find(Input::get('request_method_id'));

			$gateway = Auth::user()->gateways()->find($id);

			$gateway->name   = Input::get('name');
			$gateway->url    = Input::get('url');
			$gateway->status = Input::get('status');

			$gateway->requestMethod()->associate($requestMethod);
			
			$gateway->user()->associate(Auth::user());

			$gateway->save();

			Notification::success(Lang::get('word.success'));

			return Redirect::action('GatewayController@index');
		}
	}

	public function destroy($id)
	{
		$gateway = Auth::user()->gateways()->find($id);

		$gateway->arguments()->delete();

		$gateway->messages()->delete();

		$gateway->delete();

		Notification::success(Lang::get('word.success'));

		return Redirect::action('GatewayController@index');
	}

}
