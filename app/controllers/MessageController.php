<?php

class MessageController extends BaseController {

	public function index() {

		if (Input::has('search')) {

			$search = Str::wrap(Input::get('search'));

			$messages = Auth::user()->messages()->where('name', 'like', $search)->paginate(5);
		} else {

			$messages = Auth::user()->messages()->paginate(5);
		}

		return View::make('pages.messages.index')->with(['messages' => $messages]);
	}

	public function create()
	{
		$gateways = Auth::user()->gateways()->whereStatus(true)->lists('name', 'id');

		$contacts = Auth::user()->contacts()->whereStatus(true)->lists('name', 'id');

		return View::make('pages.messages.create')->with(['gateways' => $gateways, 'contacts' => $contacts]);
	}

	public function store()
	{
		$rules = [
			'name' => [
				'required',
				'unique:messages,name,NULL,id,deleted_at,NULL,user_id,' . Auth::user()->id
			],
			'content' => [
				'required'
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		} else {

			$gateway = Auth::user()->gateways()->find(Input::get('gateway_id'));

			$message = new Message();

			$message->name    = Input::get('name');
			$message->content = Input::get('content');
			
			$message->gateway()->associate($gateway);
			
			$message->user()->associate(Auth::user());

			$message->save();

			if (Input::has('contacts')) {

				$message->contacts()->sync(Input::get('contacts'));
			}

			Notification::success(Lang::get('word.success'));

			return Redirect::action('MessageController@index');
		}
	}

	public function edit($id)
	{
		$message = Auth::user()->messages()->with('contacts', 'gateway')->find($id);

		$gateways = Auth::user()->gateways()->whereStatus(true)->lists('name', 'id');

		$contacts = Auth::user()->contacts()->whereStatus(true)->lists('name', 'id');

		return View::make('pages.messages.edit')->with(['message' => $message, 'gateways' => $gateways, 'contacts' => $contacts]);
	}

	public function update($id)
	{
		$rules = [
			'name' => [
				'required',
				'unique:messages,name,' . $id . ',id,deleted_at,NULL,user_id,' . Auth::user()->id
			],
			'content' => [
				'required'
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		} else {

			$gateway = Auth::user()->gateways()->find(Input::get('gateway_id'));

			$message = Auth::user()->messages()->find($id);

			$message->name    = Input::get('name');
			$message->content = Input::get('content');
			
			$message->gateway()->associate($gateway);
			
			$message->user()->associate(Auth::user());

			$message->save();

			if (Input::has('contacts')) {

				$message->contacts()->sync(Input::get('contacts'));
			} else {

				$message->contacts()->detach();
			}

			Notification::success(Lang::get('word.success'));

			return Redirect::action('MessageController@index');
		}
	}

	public function destroy($id)
	{
		$message = Auth::user()->messages()->find($id);

		$message->contacts()->detach();

		$message->cronjobs()->delete();

		$message->sqlQueries()->delete();

		$message->delete();

		Notification::success(Lang::get('word.success'));

		return Redirect::action('MessageController@index');
	}

	public function send($id)
	{
		Auth::user()->messages()->find($id)->send();

		Notification::success(Lang::get('word.success'));

		return Redirect::action('MessageController@index');
	}

}
