<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>@yield('title')</title>
		@include('layouts.cosmo.styles')
		@include('layouts.cosmo.conditional-comments')
	</head>
	<body>
		@include('layouts.cosmo.navbar')
		@include('layouts.cosmo.content')
		@include('layouts.cosmo.scripts')
	</body>
</html>