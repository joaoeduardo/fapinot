<?php

class CronjobController extends BaseController {

	public function index($messageId)
	{
		$message = Auth::user()->messages()->find($messageId);

		if (Input::has('search')) {

			$search = Str::wrap(Input::get('search'));

			$cronjobs = $message->cronjobs()->where('name', 'like', $search)->paginate(5);
		} else {

			$cronjobs = $message->cronjobs()->paginate(5);
		}

		return View::make('pages.cronjobs.index')->with(['message' => $message, 'cronjobs' => $cronjobs]);
	}

	public function create($messageId)
	{
		$message = Auth::user()->messages()->find($messageId);

		return View::make('pages.cronjobs.create')->with(['message' => $message]);
	}

	public function store($messageId)
	{
		$rules = [
			'name' => [
				'required',
				'unique:cronjobs,name,NULL,id,deleted_at,NULL,user_id,' . Auth::user()->id
			],
			'text' => [
				'required',
				'cron'
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		} else {

			$message = Auth::user()->messages()->find($messageId);

			$cronjob = new Cronjob();

			$cronjob->name = Input::get('name');
			$cronjob->text = Input::get('text');
			
			$cronjob->message()->associate($message);
			
			$cronjob->user()->associate(Auth::user());

			$cronjob->save();

			Notification::success(Lang::get('word.success'));

			return Redirect::action('CronjobController@index', $message->id);
		}
	}

	public function edit($messageId, $id)
	{
		$message = Auth::user()->messages()->find($messageId);

		$cronjob = $message->cronjobs()->find($id);

		return View::make('pages.cronjobs.edit')->with(['message' => $message, 'cronjob' => $cronjob]);
	}

	public function update($messageId, $id)
	{
		$rules = [
			'name' => [
				'required',
				'unique:cronjobs,name,' . $id . ',id,deleted_at,NULL,user_id,' . Auth::user()->id
			],
			'text' => [
				'required',
				'cron'
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		} else {

			$message = Auth::user()->messages()->find($messageId);

			$cronjob = Cronjob::find($id);

			$cronjob->name = Input::get('name');
			$cronjob->text = Input::get('text');
			
			$cronjob->message()->associate($message);
			
			$cronjob->user()->associate(Auth::user());

			$cronjob->save();

			Notification::success(Lang::get('word.success'));

			return Redirect::action('CronjobController@index', $message->id);
		}
	}

	public function destroy($messageId, $id)
	{

		$message = Auth::user()->messages()->find($messageId);

		$cronjob = $message->cronjobs()->find($id);

		$cronjob->delete();

		Notification::success(Lang::get('word.success'));

		return Redirect::action('CronjobController@index', $message->id);
	}

}
