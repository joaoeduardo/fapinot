@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.gateways') }}
@stop

@section('content')
<h1>{{ Lang::get('word.gateways') }}</h1>
{{ HTML::linkAction('GatewayController@create', Lang::get('word.create'), [], ['class' => 'btn btn-success']) }}
{{ Former::inline_open()->action(Request::path())->method('get')->addClass('pull-right') }}
{{ Former::text('search')->placeholder(Lang::get('word.search')) }}
{{ Former::close() }}
<table class="table">
	<thead>
		<tr>
			<th class="col-xs-4">{{ Lang::get('word.name') }}</th class="col-xs-">
			<th class="col-xs-4">{{ Lang::get('word.status') }}</th class="col-xs-">
			<th class="col-xs-4">{{ Lang::get('word.actions') }}</th class="col-xs-">
		</tr>
	</thead>
	<tbody>
		@foreach($gateways as $gateway)
		<tr>
			<td>{{ $gateway->name }}</td>
			<td>{{ $gateway->status ? Lang::get('word.active') : Lang::get('word.inactive'); }}</td>
			<td>
				{{ Former::open()->controller('GatewayController@destroy', [$gateway->id]) }}
				{{ HTML::linkAction('ArgumentController@index', Lang::get('word.arguments'), [$gateway->id], ['class' => 'btn btn-info']) }}
				{{ HTML::linkAction('GatewayController@edit', Lang::get('word.edit'), [$gateway->id], ['class' => 'btn btn-warning']) }}
				{{ Former::danger_submit('delete') }}
				{{ Former::close() }}
			</td>
		</tr>
		@endforeach
	</tbody>
	<tfoot>
		<th colspan="4" class="text-center">
			{{ Lang::get('word.pagination', ['from' => $gateways->getFrom(), 'to' => $gateways->getTo(), 'total' => $gateways->getTotal()]) }}
		</th>
	</tfoot>
</table>
{{ $gateways->links() }}
@stop