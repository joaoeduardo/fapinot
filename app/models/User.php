<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	protected $hidden = array('password', 'remember_token');

	public function arguments()
	{
		return $this->hasMany('Argument');
	}

	public function contacts()
	{
		return $this->hasMany('Contact');
	}

	public function cronjobs()
	{
		return $this->hasMany('Cronjob');
	}

	public function gateways()
	{
		return $this->hasMany('Gateway');
	}

	public function groups()
	{
		return $this->hasMany('Group');
	}

	public function messages()
	{
		return $this->hasMany('Message');
	}

	public function sources()
	{
		return $this->hasMany('Source');
	}

	public function sqlQueries()
	{
		return $this->hasMany('SqlQuery');
	}

}
