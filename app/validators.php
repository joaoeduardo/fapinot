<?php

Validator::extend('cron', function($attribute, $value, $parameters) {

	try {

		Cron\CronExpression::factory($value);
	} catch (InvalidArgumentException $error) {
	
		return false;
	}

	return true;
});
