<?php

class DriversTableSeeder extends DatabaseSeeder {

	public function run()
	{
		Driver::firstOrCreate(['name' => 'MySQL', 'prefix' => 'mysql', 'port' => 3306]);
		Driver::firstOrCreate(['name' => 'SQL Server', 'prefix' => 'mssql', 'port' => 1433]);
	}

}
