<?php

class SqlQuery extends Eloquent {

	use SoftDeletingTrait;

	public function message()
	{
		return $this->belongsTo('Message');
	}

	public function source()
	{
		return $this->belongsTo('Source');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function execute()
	{
		return $this->source->connect()->select($this->text);
	}

}
