<?php

class ContactController extends BaseController {

	public function index()
	{
		if (Input::has('search')) {

			$search = Str::wrap(Input::get('search'));

			$contacts = Auth::user()->contacts()->with('groups')->where('name', 'like', $search)->paginate(5);
		} else {

			$contacts = Auth::user()->contacts()->with('groups')->paginate(5);
		}

		return View::make('pages.contacts.index')->with(['contacts' => $contacts]);
	}

	public function create()
	{
		$groups = Auth::user()->groups()->whereStatus(true)->lists('name', 'id');

		return View::make('pages.contacts.create')->with(['groups' => $groups]);
	}

	public function store()
	{
		$rules = [
			'name' => [
				'required',
				'unique:contacts,name,NULL,id,deleted_at,NULL,user_id,' . Auth::user()->id
			],
			'email' => [
				'email',
				'unique:contacts,email,NULL,id,deleted_at,NULL,user_id,' . Auth::user()->id
			],
			'cellphone' => [
				'required',
				'numeric',
				'digits_between:11,12',
				'unique:contacts,cellphone,NULL,id,deleted_at,NULL,user_id,' . Auth::user()->id
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		} else {

			$contact = new Contact();

			$contact->name      = Input::get('name');
			$contact->email     = Input::get('email');
			$contact->cellphone = Input::get('cellphone');
			$contact->status    = Input::get('status');

			$contact->user()->associate(Auth::user());

			$contact->save();

			if (Input::has('groups')) {

				$contact->groups()->sync(Input::get('groups'));
			}

			Notification::success(Lang::get('word.success'));

			return Redirect::action('ContactController@index');
		}
	}

	public function edit($id)
	{
		$contact = Auth::user()->contacts()->with('groups')->find($id);

		$groups = Auth::user()->groups()->whereStatus(true)->lists('name', 'id');

		return View::make('pages.contacts.edit')->with(['contact' => $contact, 'groups' => $groups]);
	}

	public function update($id)
	{
		$rules = [
			'name' => [
				'required',
				'unique:contacts,name,' . $id . ',id,deleted_at,NULL,user_id,' . Auth::user()->id
			],
			'email' => [
				'email',
				'unique:contacts,email,' . $id . ',id,deleted_at,NULL,user_id,' . Auth::user()->id
			],
			'cellphone' => [
				'required',
				'numeric',
				'digits_between:11,12',
				'unique:contacts,cellphone,' . $id . ',id,deleted_at,NULL,user_id,' . Auth::user()->id
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		} else {

			$contact = Auth::user()->contacts()->find($id);

			$contact->name      = Input::get('name');
			$contact->email     = Input::get('email');
			$contact->cellphone = Input::get('cellphone');
			$contact->status    = Input::get('status');

			$contact->user()->associate(Auth::user());

			$contact->save();

			if (Input::has('groups')) {

				$contact->groups()->sync(Input::get('groups'));
			} else {

				$contact->groups()->detach();
			}

			Notification::success(Lang::get('word.success'));

			return Redirect::action('ContactController@index');
		}
	}

	public function destroy($id)
	{
		$contact = Auth::user()->contacts()->with('groups')->find($id);

		$contact->groups()->detach();

		if(!is_null($contact->externalContact)) {

			$contact->externalContact->delete();
		}

		$contact->delete();

		Notification::success(Lang::get('word.success'));

		return Redirect::action('ContactController@index');
	}

}
