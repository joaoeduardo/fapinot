<?php

class GroupController extends BaseController {

	public function index()
	{
		if (Input::has('search')) {

			$search = Str::wrap(Input::get('search'));

			$groups = Auth::user()->groups()->where('name', 'like', $search)->paginate(5);
		} else {

			$groups = Auth::user()->groups()->paginate(5);
		}

		return View::make('pages.groups.index')->with(['groups' => $groups]);
	}

	public function create()
	{
		$contacts = Auth::user()->contacts()->whereStatus(true)->lists('name', 'id');

		return View::make('pages.groups.create')->with(['contacts' => $contacts]);
	}

	public function store()
	{
		$rules = [
			'name' => [
				'required',
				'unique:groups,name,NULL,id,deleted_at,NULL,user_id,' . Auth::user()->id
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		} else {

			$group = new Group();

			$group->name        = Input::get('name');
			$group->description = Input::get('description');
			$group->status      = Input::get('status') ? true : false;

			$group->user()->associate(Auth::user());

			$group->save();

			if (Input::has('contacts')) {

				$group->contacts()->sync(Input::get('contacts'));
			}

			Notification::success(Lang::get('word.success'));

			return Redirect::action('GroupController@index');
		}
	}

	public function edit($id)
	{
		$group = Auth::user()->groups()->find($id);

		$contacts = Auth::user()->contacts()->whereStatus(true)->lists('name', 'id');

		return View::make('pages.groups.edit')->with(['group' => $group, 'contacts' => $contacts]);
	}

	public function update($id)
	{
		$rules = [
			'name' => [
				'required',
				'unique:groups,name,' . $id . ',id,deleted_at,NULL,user_id,' . Auth::user()->id
			]
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		} else {

			$group = Auth::user()->groups()->find($id);

			$group->name        = Input::get('name');
			$group->description = Input::get('description');
			$group->status      = Input::get('status') ? true : false;

			$group->user()->associate(Auth::user());

			$group->save();

			if (Input::has('contacts')) {

				$group->contacts()->sync(Input::get('contacts'));
			} else {

				$group->contacts()->detach();
			}

			Notification::success(Lang::get('word.success'));

			return Redirect::action('GroupController@index');
		}
	}

	public function destroy($id)
	{
		$group = Auth::user()->groups()->find($id);

		$group->contacts()->detach();

		$group->delete();

		Notification::success(Lang::get('word.success'));

		return Redirect::action('GroupController@index');
	}

}
