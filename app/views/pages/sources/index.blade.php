@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.sources') }}
@stop

@section('content')
<h1>{{ Lang::get('word.sources') }}</h1>
{{ HTML::linkAction('SourceController@create', Lang::get('word.create'), [], ['class' => 'btn btn-success']) }}
{{ Former::inline_open()->action(Request::path())->method('get')->addClass('pull-right') }}
{{ Former::text('search')->placeholder(Lang::get('word.search')) }}
{{ Former::close() }}
<table class="table">
	<thead>
		<tr>
			<th class="col-xs-4">{{ Lang::get('word.name') }}</th class="col-xs-">
			<th class="col-xs-2">{{ Lang::get('word.driver') }}</th class="col-xs-">
			<th class="col-xs-2">{{ Lang::get('word.status') }}</th class="col-xs-">
			<th class="col-xs-4">{{ Lang::get('word.actions') }}</th class="col-xs-">
		</tr>
	</thead>
	<tbody>
		@foreach($sources as $source)
		<tr>
			<td>{{ $source->name }}</td>
			<td>{{ $source->driver->name }}</td>
			<td>{{ $source->status ? Lang::get('word.active') : Lang::get('word.inactive') }}</td>
			<td>
				{{ Former::open()->controller('SourceController@destroy', [$source->id]) }}
				{{ HTML::linkAction('SourceController@edit', Lang::get('word.edit'), [$source->id], ['class' => 'btn btn-warning']) }}
				{{ Former::danger_submit('delete') }}
				{{ Former::close() }}
			</td>
		</tr>
		@endforeach
	</tbody>
	<tfoot>
		<th colspan="4" class="text-center">
			{{ Lang::get('word.pagination', ['from' => $sources->getFrom(), 'to' => $sources->getTo(), 'total' => $sources->getTotal()]) }}
		</th>
	</tfoot>
</table>
{{ $sources->links() }}
@stop