@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.edit') }} {{ Lang::get('word.source') }}
@stop

@section('content')
<h1>{{ Lang::get('word.edit') }} {{ Lang::get('word.source') }}</h1>
{{ Former::open()->controller('SourceController@update', [$source->id]) }}
{{ Former::populate($source) }}
{{ Former::text('name') }}
{{ Former::text('host') }}
{{ Former::text('username') }}
{{ Former::password('password') }}
{{ Former::text('db') }}
{{ Former::radios('status')->radios('inactive', 'active') }}
{{ Former::select('driver_id')->label('driver')->options($drivers) }}
{{ Former::success_submit('save') }}
{{ Former::close() }}
@stop