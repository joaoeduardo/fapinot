<?php

View::composer(Paginator::getViewName(), function($view) {

	$query = array_except(Input::query(), Paginator::getPageName());

	$view->paginator->appends($query);
});
