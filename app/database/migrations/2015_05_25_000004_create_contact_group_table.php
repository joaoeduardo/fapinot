<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactGroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(
			'contact_group',
			function(Blueprint $table) {

				$table->bigIncrements('id');
				$table->bigInteger('contact_id')->unsigned();
				$table->foreign('contact_id')->references('id')->on('contacts');
				$table->bigInteger('group_id')->unsigned();
				$table->foreign('group_id')->references('id')->on('groups');
			}
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('contact_group');
	}

}
