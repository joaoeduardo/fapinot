@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.create') }} {{ Lang::get('word.contact') }}
@stop

@section('content')
<h1>{{ Lang::get('word.create') }} {{ Lang::get('word.contact') }}</h1>
{{ Former::open()->controller('ContactController@store') }}
{{ Former::text('name') }}
{{ Former::text('email') }}
{{ Former::text('cellphone') }}
{{ Former::radios('status')->radios('inactive', 'active')->check(1) }}
{{ Former::multiselect('groups')->options($groups) }}
{{ Former::success_submit('save') }}
{{ Former::close() }}
@stop