@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.signup') }}
@stop

@section('content')
<h1>{{ Lang::get('word.signup') }}</h1>
{{ Former::open()->controller('UserController@doSignup') }}
{{ Former::text('firstname') }}
{{ Former::text('lastname') }}
{{ Former::text('nickname') }}
{{ Former::text('email') }}
{{ Former::password('password') }}
{{ Former::password('repeat-password') }}
{{ Former::success_submit('signup') }}
{{ Former::close() }}
@stop