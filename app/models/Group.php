<?php

class Group extends Eloquent {

	use SoftDeletingTrait;

	public function contacts()
	{
		return $this->belongsToMany('Contact');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

}
