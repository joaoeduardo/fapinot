<?php

class RequestMethodsTableSeeder extends DatabaseSeeder {

	public function run()
	{
		RequestMethod::firstOrCreate(['name' => 'GET']);
		RequestMethod::firstOrCreate(['name' => 'HEAD']);
		RequestMethod::firstOrCreate(['name' => 'POST']);
		RequestMethod::firstOrCreate(['name' => 'PUT']);
		RequestMethod::firstOrCreate(['name' => 'DELETE']);
		RequestMethod::firstOrCreate(['name' => 'CONNECT']);
		RequestMethod::firstOrCreate(['name' => 'OPTIONS']);
		RequestMethod::firstOrCreate(['name' => 'TRACE']);
	}

}
