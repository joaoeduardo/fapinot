<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactMessageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(
			'contact_message',
			function(Blueprint $table) {

				$table->bigIncrements('id');
				$table->bigInteger('contact_id')->unsigned();
				$table->foreign('contact_id')->references('id')->on('contacts');
				$table->bigInteger('message_id')->unsigned();
				$table->foreign('message_id')->references('id')->on('messages');
			}
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('contact_message');
	}

}
