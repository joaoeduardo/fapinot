@extends('layouts.cosmo.main')

@section('title')
{{ Lang::get('word.create') }} {{ Lang::get('word.group') }}
@stop

@section('content')
<h1>{{ Lang::get('word.create') }} {{ Lang::get('word.group') }}</h1>
{{ Former::open()->controller('GroupController@store') }}
{{ Former::text('name') }}
{{ Former::textarea('description') }}
{{ Former::radios('status')->radios('inactive', 'active')->check(1) }}
{{ Former::multiselect('contacts')->options($contacts) }}
{{ Former::success_submit('save') }}
{{ Former::close() }}
@stop